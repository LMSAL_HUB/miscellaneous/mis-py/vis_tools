# LMSAL_HUB/vis_tools

Is a library for generic plotting, and movies and widgets from the Lockheed Martin Solar and Astrophysical Lab (LMSAL).

The library is a collection of different scripts and classes with varying degrees of portability and usefulness.

# Installation
Before attempting to install vis_tools you need the following:

       Python (2.7.x, 3.4.x or later)
       NumPy
       SciPy
       Matplotlib (1.1+)
       Pylab
       mpl_toolkit
       IPython.display
       cv2
       functools
       fnmatch

The following packages are also recommended to take advantage of all the features:


Most of the above Python packages are available through Anaconda, and that is the recommended way of setting up your Python distribution.

Next, use git to grab the latest version of vis_tools:

	  mkdir -p LMSAL_HUB/miscellaneous/mis-py
	  cd LMSAL_HUB/miscellaneous/mis-py
	  git clone https://gitlab.com/LMSAL_HUB/miscellaneous/mis-py/vis_tools.git
	  cd vis_tools
	  python setup.py install

###     Non-root install

If you do not have write permission to your Python packages directory, use the following option with setup.py:

        python setup.py install --user

This will install LMSAL_HUB/vis_tools under your home directory (typically ~/.local).

###     Developer install

If you want to install vis_tools but also actively change the code or contribute to its development, it is recommended that you do a developer install instead:

        python setup.py develop

This will set up the package such as the source files used are from the git repository that you cloned (only a link to it is placed on the Python packages directory). Can also be combined with the --user flag for local installs.

# Documentation

Some form of documentation will be made available at http://readthedocs.org, but right now there is little documentation other than that in docstrings.
