import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from matplotlib.image import NonUniformImage
from matplotlib import cm, colors
import pylab
from IPython.display import display, Math, Latex
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable

def verimage(spltx, splty, spltp, image, x, y, xlim=None, ylim=None,
             vmin=None, vmax=None, title='', xlabel='', ylabel='', colbar=None,
             cbtitle='', cmap=cm.jet, pad="17%", **kargs):

    if xlim != None:
        xmin=np.max((x.min(),xlim[0]))
        xmax=np.min((x.max(),xlim[1]))
    else:
        xmin=x.min()
        xmax=x.max()
    if ylim != None:
        ymin=np.max((-y.max(),ylim[0]))
        ymax=np.min((-y.min(),ylim[1]))
    else:
        ymin=-y.max()
        ymax=-y.min()
    extent=(xmin,xmax,ymin,ymax)

    if (vmax==None):
        vmax=np.max(image)
    if (vmin==None):
        vmin=np.min(image)
    ax = plt.subplot(spltx,splty,spltp)
    im = NonUniformImage(ax,interpolation='bilinear',extent=extent,cmap=cmap,**kargs)
    norm = colors.Normalize(vmin=vmin, vmax=vmax)
    im.set_norm(norm)
    im.set_data(x,-y[::-1],np.flipud(np.fliplr(list(zip(*image[::-1])))))
    ax.images.append(im)
    if (xlim == None):
        xlim = (x.min(),x.max())
    if (ylim == None):
        ylim = (y.max(),-y.min())
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_aspect('equal')#(xlim[1]-xlim[0])/(ylim[1]-ylim[0]))
    if colbar == 'top':
        ax2_divider = make_axes_locatable(ax)
        cax2 = ax2_divider.append_axes("top", size="4%", pad=pad)
        cax2.set_title(cbtitle)
        cbar=plt.colorbar(im,cax=cax2, orientation='horizontal')
    elif colbar == 'bottom':
        orientation = 'horizontal'
        plt.colorbar(im,orientation=orientation,set_label=cbtitle) #, labelpad=-1 move labels closer with -
    elif colbar == 'left':
        ax2_divider = make_axes_locatable(ax)
        cax2 = ax2_divider.append_axes("bottom", size="4%", pad=pad)
        cax2.set_title(cbtitle)
        cbar=plt.colorbar(im,cax=cax2, orientation='horizontal')
    elif colbar != None:
        #ax2_divider = make_axes_locatable(ax)
        #cax2 = ax2_divider.append_axes("right", size="4%", pad=pad)
        #cax2.set_title(cbtitle, rotation=-90)
        cbar=plt.colorbar(im)
        cbar.set_label(cbtitle)

    if (xlabel != ''):
        ax.set_xlabel(xlabel)
    if (ylabel != ''):
        ax.set_ylabel(ylabel)
    if (title != ''):
        ax.set_title(title)
    return ax


def verimagegrd(splt, image, x, y, xlim=None, ylim=None,
             vmin=None, vmax=None, title='', xlabel='', ylabel='', colbar=None,
             cbtitle='',cmap=cm.jet, pad="17%", **kargs):

    if xlim != None:
        xmin=np.max((x.min(),xlim[0]))
        xmax=np.min((x.max(),xlim[1]))
    else:
        xmin=x.min()
        xmax=x.max()
    if ylim != None:
        ymin=np.max((-y.max(),ylim[0]))
        ymax=np.min((-y.min(),ylim[1]))
    else:
        ymin=-y.max()
        ymax=-y.min()
    extent=(xmin,xmax,ymin,ymax)

    if (vmax==None):
        vmax=np.max(image)
    if (vmin==None):
        vmin=np.min(image)
    ax = plt.subplot(splt)
    im = NonUniformImage(ax,interpolation='bilinear',extent=extent,cmap=cmap,**kargs)
    norm = colors.Normalize(vmin=vmin, vmax=vmax)
    im.set_norm(norm)
    im.set_data(x,-y[::-1],np.flipud(np.fliplr(list(zip(*image[::-1])))))
    ax.images.append(im)
    if (xlim == None):
        xlim = (x.min(),x.max())
    if (ylim == None):
        ylim = (y.max(),-y.min())
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_aspect('equal')#(xlim[1]-xlim[0])/(ylim[1]-ylim[0]))
    if colbar == 'top':
        ax2_divider = make_axes_locatable(ax)
        cax2 = ax2_divider.append_axes("top", size="4%", pad=pad,)
        cax2.set_title(cbtitle)
        cbar=plt.colorbar(im,cax=cax2, orientation='horizontal')
    elif colbar == 'bottom':
        orientation = 'horizontal'
        plt.colorbar(im,orientation=orientation,set_label=cbtitle) #, labelpad=-1 move labels closer with -
    elif colbar == 'left':
        ax2_divider = make_axes_locatable(ax)
        cax2 = ax2_divider.append_axes("bottom", size="4%", pad=pad,)
        cax2.set_title(cbtitle)
        cbar=plt.colorbar(im,cax=cax2, orientation='horizontal')
    elif colbar != None:
        plt.colorbar(im)

    if (xlabel != ''):
        ax.set_xlabel(xlabel)
    if (ylabel != ''):
        ax.set_ylabel(ylabel)
    if (title != ''):
        ax.set_title(title)
    return ax
