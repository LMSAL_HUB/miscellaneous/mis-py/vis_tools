
import cv2
import os
import fnmatch
import functools
from functools import cmp_to_key
import sys

#Function to check if string can be cast to int
def isnum (num):
    try:
            int(num)
            return True
    except:
            return False

#Numerically sorts filenames
def image_sort (x,y):
    x = int(x.split(".")[0])
    y = int(y.split(".")[0])
    return x-y

def create_mpg(rootname=None, output=None, ext='png', dir_path='.', codec='avc1',
               framerate=10, sort_type='numeric', time=None, visual=False):

    if output is None:
        if codec == 'avc1':
            output='output.mp4'
        else:
            output='output.m4v'

    #Get the files from directory
    images = []
    for f in os.listdir(dir_path):
        if f.endswith(ext):
            if rootname is None:
                images.append(f)
            else:
                if fnmatch.fnmatch(f, rootname+'*'+ext):
                    images.append(f)

    #Sort the files found in the directory
    if images == []: 
        print('No files with format %s and rootname %s' %(ext,rootname),sys.exc_info()[0])
        raise 

    if sort_type == "numeric":
        int_name = images[0].split(".")[0]
        if isnum(int_name):
                images = sorted(images, key=cmp_to_key(image_sort))
        else:
                print("Failed to sort numerically, switching to alphabetic sort")
                images.sort()
    elif sort_type == "alphabetic":
        images.sort()

    #Change framerate to fit the time in seconds if a time has been specified.
    #Overrides the -fps arg
    if time is not None:
        framerate = int(len(images) / int(time))
        print("Adjusting framerate to " + str(framerate))

    # Determine the width and height from the first image
    image_path = os.path.join(dir_path, images[0])
    frame = cv2.imread(image_path)

    if visual:
        cv2.imshow('video',frame)
    regular_size = os.path.getsize(image_path)
    height, width, channels = frame.shape

    # Define the codec and create VideoWriter object
    #fourcc = cv2.VideoWriter_fourcc(*'mp4v') # Be sure to use lower case # 0x00000020
    #fourcc = cv2.VideoWriter_fourcc(*'avc1') # Be sure to use lower case # 0x00000021
    #out = cv2.VideoWriter(output, fourcc, framerate, (width, height))
    if codec == 'avc1':
        fourcc = cv2.VideoWriter_fourcc(*'avc1')
        #out = cv2.VideoWriter(output, 0x00000021, framerate, (width, height))
        out = cv2.VideoWriter(output, fourcc, framerate, (width, height))
    else:
        out = cv2.VideoWriter(output, 0x00000020, framerate, (width, height))

    for n, image in enumerate(images):
        image_path = os.path.join(dir_path, image)
        image_size = os.path.getsize(image_path)
        if image_size < regular_size / 1.5:
                print("Cancelled: " + image)
                continue

        frame = cv2.imread(image_path)
        out.write(frame) # Write out frame to video
        if visual:
            cv2.imshow('video', frame)
            if (cv2.waitKey(1) & 0xFF) == ord('q'): # Hit `q` to exit
                break

        if n%50 == 0:
                print("Frame " + str(n))

    # Release everything if job is finished
    out.release()
    cv2.destroyAllWindows()
