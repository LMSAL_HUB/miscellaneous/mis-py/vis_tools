import os
import setuptools
import numpy as np
from numpy.distutils.core import setup

NAME = "visualization_tools"
PACKAGES = ["plot_tools","movie_tools","widget_tools"]
ALL_PACKAGES = ["%s" %  package for package in PACKAGES]


VERSION = "0.0"

setup(
    name=NAME,
    version=VERSION,
    description="",
    author="Juan Martinez-Sykora et al.",
    license="BSD",
    url="http://%s.readthedocs.io" % NAME,
    keywords=['astronomy', 'astrophysics', 'solar physics', 'space', 'science'],
    classifiers=[
          'Intended Audience :: Science/Research',
          'License :: OSI Approved :: BSD License',
          'Operating System :: OS Independent',
          'Programming Language :: Python :: 3',
          'Topic :: Scientific/Engineering :: Astronomy',
          'Topic :: Scientific/Engineering :: Physics'
    ],
    packages=ALL_PACKAGES,
    python_requires='>=3.0',
)
